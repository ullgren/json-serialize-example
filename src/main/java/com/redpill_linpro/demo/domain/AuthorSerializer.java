package com.redpill_linpro.demo.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class AuthorSerializer extends JsonSerializer<Author> {
	
	@Override
	public void serialize(Author author, JsonGenerator jgen,
			SerializerProvider sp) throws IOException,
			JsonProcessingException {
		
		jgen.writeStartObject();
        jgen.writeStringField("href", "/jsontest/author/"+ author.getId());
        jgen.writeEndObject();
		
	}

}

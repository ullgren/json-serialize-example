package com.redpill_linpro.demo.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class Book {
	
	@Id
	private String isbn;
	
	private String title;
	
	private String signum;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "book_author", 
		joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "isbn")}, 
		inverseJoinColumns = {@JoinColumn(name = "author_id", referencedColumnName = "id")})
	@JsonSerialize(contentUsing=AuthorSerializer.class)
	private Set<Author> authors;
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getIsbn() {
		return isbn;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	
	public void setSignum(String signum) {
		this.signum = signum;
	}
	public String getSignum() {
		return signum;
	}
	
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}
	public Set<Author> getAuthors() {
		return authors;
	}
	
	
}

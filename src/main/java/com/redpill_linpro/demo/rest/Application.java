package com.redpill_linpro.demo.rest;

import io.swagger.jaxrs.config.BeanConfig;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;

/**
 * @author andreas.arvidsson@redpill-linpro.com
 *
 *
 */
@ApplicationPath("rest")
public class Application extends javax.ws.rs.core.Application {

    public Application() {
        //Swagger setup.
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("rest");
        beanConfig.setResourcePackage("com.redpill_linpro.demo.rest");
        beanConfig.setScan(true);
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(BookService.class);
        classes.add(AuthorService.class);
        
        classes.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        classes.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

        return classes;
    }

}

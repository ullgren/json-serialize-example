package com.redpill_linpro.demo.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.redpill_linpro.demo.domain.Book;

@Path("book")
@Transactional
@Api(value = "book")
public class BookService {
	
	@PersistenceContext
    private EntityManager em;

    @ApiOperation(value = "Get books.",
            response = Book.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "200(OK), list of books returned."),
        @ApiResponse(code = 204, message = "204(No content), no books found.")})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SuppressWarnings("unchecked")
    public Response getBooks() throws Exception {
    	List<Book> books = em.createQuery("SELECT b FROM Book b").getResultList();
    	return Response.ok(books).build();
    }
    
    

}

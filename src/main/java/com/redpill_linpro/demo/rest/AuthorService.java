package com.redpill_linpro.demo.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.redpill_linpro.demo.domain.Author;
import com.redpill_linpro.demo.domain.Book;

@Path("author")
@Transactional
@Api(value = "author")
public class AuthorService {
	
	@PersistenceContext
    private EntityManager em;

    @ApiOperation(value = "Get authors.",
            response = Book.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "200(OK), list of authors returned."),
        @ApiResponse(code = 204, message = "204(No content), no authors found.")})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SuppressWarnings("unchecked")
    public Response getAuthos() throws Exception {
    	List<Author> authors = em.createQuery("SELECT a FROM Author a").getResultList();
    	return Response.ok(authors).build();
    }
    
    

}
